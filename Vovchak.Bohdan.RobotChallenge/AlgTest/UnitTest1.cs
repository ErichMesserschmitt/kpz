﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Tracing;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Robot.Common;

namespace AlgTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void testFindStation()
        {
            Vovchak.Bohdan.RobotChallenge.VovchakAlgorithm alg = new Vovchak.Bohdan.RobotChallenge.VovchakAlgorithm();
            Map map = new Map();
            List<Robot.Common.Robot> list = new List<Robot.Common.Robot>();
            list.Add(new Robot.Common.Robot() { Position = new Position(1, 1), Energy = 400 });

            List<EnergyStation> elist = new List<EnergyStation>();
            elist.Add(new EnergyStation { Energy = 400, Position = new Position { X = 10, Y = 10 }, RecoveryRate = 50 });
            elist.Add(new EnergyStation { Energy = 300, Position = new Position { X = 15, Y = 15 }, RecoveryRate = 50 });

            map.Stations = elist;
            EnergyStation eng = alg.FindNearestFreeStation(list[0], map, list, 0);
            int energy = 400;
            Assert.AreEqual(energy, eng.Energy);
        }
        [TestMethod]
        public void testStepCalculation0()
        {
            Position x = new Position(10, 10);
            Robot.Common.Robot a = new Robot.Common.Robot() { Position = new Position(0, 0), Energy = 200 };
            int steps = Vovchak.Bohdan.RobotChallenge.Utils.calculateSteps(a, x);
            Assert.AreEqual(steps, 2);
        }

        [TestMethod]
        public void testStepCalculation4()
        {
            Position x = new Position(10, 10);
            Robot.Common.Robot a = new Robot.Common.Robot() { Position = new Position(0, 0), Energy = 100 };
            int steps = Vovchak.Bohdan.RobotChallenge.Utils.calculateSteps(a, x);
            Assert.AreEqual(steps, 4);
        }

        [TestMethod]
        public void testStepCalculation6()
        {
            Position x = new Position(10, 10);
            Robot.Common.Robot a = new Robot.Common.Robot() { Position = new Position(0, 0), Energy = 80 };
            int steps = Vovchak.Bohdan.RobotChallenge.Utils.calculateSteps(a, x);
            Assert.AreEqual(steps, 6);
        }

        [TestMethod]
        public void stationCheckerTest1()
        {

            Map map = new Map();
            List<Robot.Common.Robot> list = new List<Robot.Common.Robot>();
            list.Add(new Robot.Common.Robot() { Position = new Position(1, 1), Energy = 400 });

            List<EnergyStation> elist = new List<EnergyStation>();
            elist.Add(new EnergyStation { Energy = 400, Position = new Position { X = 1, Y = 1 }, RecoveryRate = 50 });
            elist.Add(new EnergyStation { Energy = 300, Position = new Position { X = 1, Y = 5 }, RecoveryRate = 50 });

            map.Stations = elist;
            Vovchak.Bohdan.RobotChallenge.Utils.generatorKarte.Add(elist[0].Position, 1);
            int count = Vovchak.Bohdan.RobotChallenge.Utils.checkStation(elist[0], list, map);
            Assert.AreEqual(count, 1);
        }
        [TestMethod]
        public void stationCheckerTest2()
        {

            Map map = new Map();
            List<Robot.Common.Robot> list = new List<Robot.Common.Robot>();
            list.Add(new Robot.Common.Robot() { Position = new Position(1, 1), Energy = 400 });

            List<EnergyStation> elist = new List<EnergyStation>();
            elist.Add(new EnergyStation { Energy = 400, Position = new Position { X = 1, Y = 1 }, RecoveryRate = 50 });
            elist.Add(new EnergyStation { Energy = 300, Position = new Position { X = 1, Y = 0 }, RecoveryRate = 50 });

            map.Stations = elist;
            Vovchak.Bohdan.RobotChallenge.Utils.generatorKarte.Remove(elist[0].Position);
            Vovchak.Bohdan.RobotChallenge.Utils.generatorKarte.Add(elist[0].Position, 2);
            int count = Vovchak.Bohdan.RobotChallenge.Utils.checkStation(elist[0], list, map);
            Assert.AreEqual(count, 2);
        }

        [TestMethod]
        public void cellChecker()
        {
            Vovchak.Bohdan.RobotChallenge.VovchakAlgorithm alg = new Vovchak.Bohdan.RobotChallenge.VovchakAlgorithm();
            Map map = new Map();
            List<Robot.Common.Robot> list = new List<Robot.Common.Robot>();
            list.Add(new Robot.Common.Robot() { Position = new Position(1, 1), Energy = 400 });
            list.Add(new Robot.Common.Robot() { Position = new Position(3, 3), Energy = 400 });
            bool res = alg.IsCellFree(new Position(3, 3), list[0], list);
            Assert.AreEqual(res, false);
        }

        [TestMethod]
        public void checkerIsOnStation()
        {
            Vovchak.Bohdan.RobotChallenge.VovchakAlgorithm alg = new Vovchak.Bohdan.RobotChallenge.VovchakAlgorithm();
            Map map = new Map();
            List<Robot.Common.Robot> list = new List<Robot.Common.Robot>();
            list.Add(new Robot.Common.Robot() { Position = new Position(1, 1), Energy = 400 });
            list.Add(new Robot.Common.Robot() { Position = new Position(3, 3), Energy = 400 });
            List<EnergyStation> elist = new List<EnergyStation>();
            elist.Add(new EnergyStation { Energy = 400, Position = new Position { X = 1, Y = 3 }, RecoveryRate = 50 });
            elist.Add(new EnergyStation { Energy = 300, Position = new Position { X = 15, Y = 15 }, RecoveryRate = 50 });
            EnergyStation a = Vovchak.Bohdan.RobotChallenge.Utils.checkIsOnStation(list[0], elist);
            Assert.AreEqual(400, a.Energy);

        }
        [TestMethod]
        public void checkerIsNotOnStation()
        {
            Vovchak.Bohdan.RobotChallenge.VovchakAlgorithm alg = new Vovchak.Bohdan.RobotChallenge.VovchakAlgorithm();
            Map map = new Map();
            List<Robot.Common.Robot> list = new List<Robot.Common.Robot>();
            list.Add(new Robot.Common.Robot() { Position = new Position(1, 1), Energy = 400 });
            list.Add(new Robot.Common.Robot() { Position = new Position(3, 3), Energy = 400 });
            List<EnergyStation> elist = new List<EnergyStation>();
            elist.Add(new EnergyStation { Energy = 400, Position = new Position { X = 8, Y = 3 }, RecoveryRate = 50 });
            elist.Add(new EnergyStation { Energy = 300, Position = new Position { X = 15, Y = 15 }, RecoveryRate = 50 });
            EnergyStation a = Vovchak.Bohdan.RobotChallenge.Utils.checkIsOnStation(list[0], elist);
            Assert.AreEqual(null, a);

        }

        [TestMethod]
        public void selectBestStationChecker()
        {
            Vovchak.Bohdan.RobotChallenge.VovchakAlgorithm alg = new Vovchak.Bohdan.RobotChallenge.VovchakAlgorithm();
            Map map = new Map();
            List<Robot.Common.Robot> list = new List<Robot.Common.Robot>();
            list.Add(new Robot.Common.Robot() { Position = new Position(1, 1), Energy = 400 });
            list.Add(new Robot.Common.Robot() { Position = new Position(3, 3), Energy = 400 });
            List<EnergyStation> elist = new List<EnergyStation>();
            elist.Add(new EnergyStation { Energy = 400, Position = new Position { X = 5, Y = 3 }, RecoveryRate = 50 });
            elist.Add(new EnergyStation { Energy = 300, Position = new Position { X = 1, Y = 2 }, RecoveryRate = 50 });
            Vovchak.Bohdan.RobotChallenge.Utils.generatorKarte.Remove(elist[0].Position);
            Vovchak.Bohdan.RobotChallenge.Utils.generatorKarte.Add(elist[0].Position, 2);
            Vovchak.Bohdan.RobotChallenge.Utils.generatorKarte.Add(elist[1].Position, 0);


            EnergyStation a = Vovchak.Bohdan.RobotChallenge.Utils.selectBestStation(elist, list, map);
            Assert.AreEqual(300, a.Energy);

        }
    }
}
