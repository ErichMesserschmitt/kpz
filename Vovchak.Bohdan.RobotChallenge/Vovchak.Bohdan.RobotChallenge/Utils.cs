﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Robot.Common;

namespace Vovchak.Bohdan.RobotChallenge
{
    public static class Utils
    {
        public static Dictionary<int, EnergyStation> zielKarte = new Dictionary<int, EnergyStation>();
        public static Dictionary<Position, int> generatorKarte = new Dictionary<Position, int>();
        public static Position calculateNextPosition(Robot.Common.Robot robot, Position position)
        {
            Position from = robot.Position;
            int steps = calculateSteps(robot, position);
            Position destination = new Position(from.X + (position.X - from.X) / steps, from.Y + (position.Y - from.Y) / steps);
            return destination;
        }

        public static bool stop = false;
        public static int calculateSteps(Robot.Common.Robot robot, Position station)
        {
            return calculateSteps_internal(robot, 1, station);
        }
        private static int calculateSteps_internal(Robot.Common.Robot robot, int divider, Position station)
        {
            Position intermediate = null;
            if (divider == 1)
            {
                intermediate = station;
            } else
            {
                int interm_X = (station.X - robot.Position.X) / divider;
                int interm_Y = (station.Y - robot.Position.Y) / divider;

                intermediate = new Position((robot.Position.X + interm_X), (robot.Position.Y + interm_Y));
            }
            int energy = (energyToSpend(robot.Position, intermediate)*divider) + 50;
           
          
            if(divider > 40)
            {
                stop = true;
            }
            if(robot.Energy < 10 || stop)
            {
                stop = false; 
                return 1;
            }
            if(energy > robot.Energy || energy > 300)
            {
                ++divider;
                return calculateSteps_internal(robot, divider, station);
            } else
            {
                return divider;
            }
        }
        public static int energyToSpend(Position from, Position to)
        {
            return (int)(Math.Pow(from.X - to.X, 2) + Math.Pow(from.Y - to.Y, 2));
        }





        public static IList<EnergyStation> findNearestStations(Robot.Common.Robot movingRobot, Robot.Common.Map map, IList<Robot.Common.Robot> robots)
        {
            IList<EnergyStation> stations = map.Stations;
            IList<EnergyStation> readyStations = new List<EnergyStation>();
            foreach (EnergyStation station in stations)
            {
                if (Math.Abs(movingRobot.Position.X - station.Position.X) <= 100 && Math.Abs(movingRobot.Position.Y - station.Position.Y) <= 100)
                {
                    int robotCount = 0;
                    if (generatorKarte.TryGetValue(station.Position, out robotCount))
                    {
                        if (robotCount >= 2)
                            continue;
                    }
                    readyStations.Add(station);
                }
            }
            return readyStations;
        }

        public static int checkStation(EnergyStation station, IList<Robot.Common.Robot> robots, Map map)
        {
            Position position = new Position(station.Position.X, station.Position.Y);
            int counter = 0;
            foreach(Robot.Common.Robot robot in robots)
            {
                if((Math.Abs(robot.Position.X - position.X) <= 2) && (Math.Abs(robot.Position.Y - position.Y) <= 2)){
                    ++counter;
                }
            }
            int robotCounter = 0;
            generatorKarte.TryGetValue(station.Position, out robotCounter);
         
            return robotCounter;
        }

        public static EnergyStation selectBestStation(IList<EnergyStation> stations, IList<Robot.Common.Robot> robots, Map map)
        {
            EnergyStation bestStation = stations[0];
            int bestStationRobotsCount = 0;
            foreach(EnergyStation station in stations)
            {
                int robotCount = 0;
                if (generatorKarte.TryGetValue(station.Position, out robotCount))
                {
                    if (robotCount >= 1)
                        continue;
                }
                int count = checkStation(station, robots, map);
                
                if(count <= 1)
                {
                    bestStation = station;
                    bestStationRobotsCount = count;
                }
                
            }
            return bestStation;

        }

        

        public static EnergyStation checkIsOnStation(Robot.Common.Robot robot, IList<EnergyStation> stations)
        {
            foreach(EnergyStation station in stations)
            {
                if(Math.Abs(robot.Position.X - station.Position.X) <=2 && Math.Abs(robot.Position.Y - station.Position.Y) <= 2){
                  
                    return station;
                }
            }
            return null;
        }

        public static IList<EnergyStation> checkNearStations(Robot.Common.Robot movingRobot, Robot.Common.Map map, IList<Robot.Common.Robot> robots, EnergyStation station_m)
        {
            IList<EnergyStation> stations = map.Stations;
            IList<EnergyStation> readyStations = new List<EnergyStation>();
            IList<EnergyStation> readyStations1 = new List<EnergyStation>();

            foreach (EnergyStation station in stations)
            {
                if (Math.Abs(movingRobot.Position.X - station.Position.X) <= 15 && Math.Abs(movingRobot.Position.Y - station.Position.Y) <= 15)
                {
                    int robotCount = 0;
                    if (generatorKarte.TryGetValue(station.Position, out robotCount))
                    {
                        if (robotCount >= 2)
                            continue;
                    }
                    readyStations.Add(station);
                }
            }
            foreach (EnergyStation station in readyStations)
            {
                if(station.Energy > station_m.Energy*2 && station.Energy > 200)
                {
                    readyStations1.Add(station);
                }
            }

                return readyStations1;
        }

    }
    

}
