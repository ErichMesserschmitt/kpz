﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Robot.Common;

namespace Vovchak.Bohdan.RobotChallenge
{
    public  class VovchakAlgorithm : IRobotAlgorithm
    {
        public int robotsc = 10;
        public string Author
        {
            get
            {
                return "Bohdan Vovchak Pz.IV";
            }
        }

        public string Description
        {
            get
            {
                return "Nothing here";
            }
        }
       

        public EnergyStation FindNearestFreeStation(Robot.Common.Robot movingRobot, Map map, IList<Robot.Common.Robot> robots, int count)
        {
            EnergyStation nearest = null;
            int minDistance = int.MaxValue;
            foreach (var station in map.Stations)
            {
                int coutn = 0;
                Utils.generatorKarte.TryGetValue(station.Position, out coutn);
                if (coutn <= count)
                {
                 
                    int d = Utils.energyToSpend(station.Position, movingRobot.Position);
                    if (d < minDistance)
                    {
                        minDistance = d;
                        nearest = station;
                    }
                }
            }
            if (nearest == null)
            {
                if (count < 4)
                {
                    nearest = FindNearestFreeStation(movingRobot, map, robots, count + 1);
                }
            }
            return nearest == null ? null : nearest;
        }
        public bool IsStationFree(EnergyStation station, Robot.Common.Robot movingRobot,
        IList<Robot.Common.Robot> robots)
        {
            return IsCellFree(station.Position, movingRobot, robots);
        }
        public bool IsCellFree(Position cell, Robot.Common.Robot movingRobot, IList<Robot.Common.Robot> robots)
        {
            foreach (var robot in robots)
            {
                if (robot != movingRobot)
                {
                    if (robot.Position == cell)
                        return false;
                }
            }
            return true;
        }

        public RobotCommand DoStep(IList<Robot.Common.Robot> robots, int robotToMoveIndex, Map map)
        {
            IList<EnergyStation> checkedSt = null;
            Robot.Common.Robot movingRobot = robots[robotToMoveIndex];
            if (movingRobot == null)
            {
                return null;
            }
            EnergyStation currentStation = Utils.checkIsOnStation(movingRobot, map.Stations);
            
            if (currentStation != null)
            {
                EnergyStation parentStation = null;
                bool istZiel = Utils.zielKarte.TryGetValue(robotToMoveIndex, out parentStation);
                if (movingRobot.Energy > 300)
                {
                   
                    if (istZiel)
                    {
                        //Utils.generatorKarte.TryGetValue(parentStation.Position, out robotCount);
                        // --robotCount;
                        // Utils.generatorKarte.Remove(parentStation.Position);
                        // Utils.generatorKarte.Add(parentStation.Position, robotCount);
                        //Utils.zielKarte.Remove(robotToMoveIndex);
                    }
                    if (map.Stations.Count*0.75 > robotsc && robotsc < 100)
                    {
                        robotsc++;
                        return new CreateNewRobotCommand() { NewRobotEnergy = movingRobot.Energy - 100 };
                    }
                }
                else
                {
                    if (istZiel)
                    {
                       
                            return new CollectEnergyCommand();
                    }

                }
            }
            EnergyStation station = null;
            if (!Utils.zielKarte.TryGetValue(robotToMoveIndex, out station))
            {
                IList<EnergyStation> stations = Utils.findNearestStations(movingRobot, map, robots);
                station = Utils.selectBestStation(stations, robots, map);
               
                station = FindNearestFreeStation(movingRobot, map, robots, 0);
                
                Utils.zielKarte.Add(robotToMoveIndex, station);
                int robotCount = 1;
                if (Utils.generatorKarte.TryGetValue(station.Position, out robotCount))
                {
                    ++robotCount;
                    Utils.generatorKarte.Remove(station.Position);
                }
                else
                {
                    robotCount = 1;
                }
                Utils.generatorKarte.Add(station.Position, robotCount);
            }
            Position nextPosition = Utils.calculateNextPosition(movingRobot, station.Position);
            if(nextPosition == null)
            {
                nextPosition = station.Position;
            }
            if(nextPosition == station.Position)
            {
                int count = 0;
                if(Utils.generatorKarte.TryGetValue(station.Position, out count))
                {
                    if(count >= 3)
                    {
                        --nextPosition.X;
                    }
                }
               
            }

            if (station.Position == null)
            {
                return null;
            }

            if(!IsCellFree(nextPosition, movingRobot, robots))
            {
                nextPosition.Y--;
            }
            if(movingRobot.Position == nextPosition)
            {
                return new CollectEnergyCommand();
            }
            return new MoveCommand() { NewPosition = nextPosition };
        }
    }
}
